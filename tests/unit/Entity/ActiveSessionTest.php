<?php

/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\ActiveSessionBundle\Tests\unit\Entity;

use Codeception\Test\Unit;
use Ipnoz\ActiveSessionBundle\Entity\ActiveSession;
use Ipnoz\ActiveSessionBundle\Tests\TestsApp\Entity\TestUser;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ActiveSessionTest extends Unit
{
    public function test_constructor_set_parameters(): void
    {
        $activeSession = new ActiveSession();
        $this->assertIsString($activeSession->getToken());
        $this->assertGreaterThanOrEqual(10, \strlen($activeSession->getToken()));
    }

    public function test_getters_and_setters(): void
    {
        $activeSession = new ActiveSession();

        $activeSession->setUser($user = new TestUser());
        $activeSession->setIp($ip = '127.0.0.42');
        $activeSession->setUserAgent($userAgent = 'user-agent');
        $activeSession->setDeactivatedAt($deactivatedAt = new \DateTime('2018-07-15'));
        $activeSession->setBlockedAt($blockedAt = new \DateTime('2018-07-15'));
        $activeSession->setCreatedAt($createdAt = new \DateTime('2018-07-15'));

        $this->assertSame($user, $activeSession->getUser());
        $this->assertSame($ip, $activeSession->getIp());
        $this->assertSame($userAgent, $activeSession->getUserAgent());
        $this->assertSame($deactivatedAt, $activeSession->getDeactivatedAt());
        $this->assertSame($blockedAt, $activeSession->getBlockedAt());
        $this->assertSame($createdAt, $activeSession->getCreatedAt());
    }

    public function test_is_blocked_method(): void
    {
        $activeSession = new ActiveSession();
        $this->assertFalse($activeSession->isBlocked());

        $activeSession->setBlockedAt(new \DateTime('+1 hour'));
        $this->assertFalse($activeSession->isBlocked());

        $activeSession->setBlockedAt(new \DateTime('-2 seconds'));
        $this->assertTrue($activeSession->isBlocked());
    }

    public function test_block_method(): void
    {
        $activeSession = new ActiveSession();

        $activeSession->block();

        $this->assertTrue($activeSession->isBlocked());
    }

    public function test_unblock_method(): void
    {
        $activeSession = new ActiveSession();

        $activeSession->unblock();

        $this->assertFalse($activeSession->isBlocked());
    }

    public function test_is_active_method(): void
    {
        $activeSession = new ActiveSession();

        $activeSession->setDeactivatedAt(new \DateTime('+1 hour'));
        $this->assertTrue($activeSession->isActive());

        $activeSession->setDeactivatedAt(new \DateTime('-2 seconds'));
        $this->assertFalse($activeSession->isActive());
    }

    public function test_is_deactivate_method(): void
    {
        $activeSession = new ActiveSession();

        $activeSession->setDeactivatedAt(new \DateTime('+1 hour'));
        $this->assertFalse($activeSession->isDeactivated());

        $activeSession->setDeactivatedAt(new \DateTime('-2 seconds'));
        $this->assertTrue($activeSession->isDeactivated());
    }

    public function test_activate_method(): void
    {
        $activeSession = new ActiveSession();
        $activeSession->setDeactivatedAt(new \DateTime('-3 days'));
        $this->assertFalse($activeSession->isActive());

        $activeSession->activate();

        $this->assertTrue($activeSession->isActive());
    }

    public function test_deactivate_method(): void
    {
        $activeSession = new ActiveSession();
        $this->assertFalse($activeSession->isDeactivated());

        $activeSession->deactivate();

        $this->assertTrue($activeSession->isDeactivated());
    }

    public function test_will_be_blocked_method(): void
    {
        $activeSession = new ActiveSession();
        $activeSession->setBlockedAt(new \DateTime('+2 days'));
        $this->assertTrue($activeSession->willBeBlocked());

        $activeSession = new ActiveSession();
        $activeSession->setBlockedAt(new \DateTime('-2 days'));
        $this->assertFalse($activeSession->willBeBlocked());

        $activeSession = new ActiveSession();
        $this->assertFalse($activeSession->willBeBlocked());
    }

    public function test_is_authorized_session_method(): void
    {
        $activeSession = new ActiveSession();
        $this->assertTrue($activeSession->isAuthorizedSession());

        $activeSession = new ActiveSession();
        $activeSession->setBlockedAt(new \DateTime('-3 days'));
        $this->assertFalse($activeSession->isAuthorizedSession());

        $activeSession = new ActiveSession();
        $activeSession->setBlockedAt(new \DateTime('+3 days'));
        $this->assertFalse($activeSession->isAuthorizedSession());

        $activeSession = new ActiveSession();
        $activeSession->setDeactivatedAt(new \DateTime('-3 days'));
        $this->assertFalse($activeSession->isAuthorizedSession());

        $activeSession = new ActiveSession();
        $activeSession->setDeactivatedAt(new \DateTime('+3 days'));
        $this->assertTrue($activeSession->isAuthorizedSession());
    }
}
