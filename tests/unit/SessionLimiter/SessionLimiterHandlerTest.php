<?php

/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\ActiveSessionBundle\Tests\unit\SessionLimiter;

use Codeception\Test\Unit;
use Ipnoz\ActiveSessionBundle\SessionLimiter\SimpleSessionLimiter;
use Ipnoz\ActiveSessionBundle\SessionLimiter\SessionLimiterHandler;
use Ipnoz\ActiveSessionBundle\Tests\UnitTester;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class SessionLimiterHandlerTest extends Unit
{
    /** @var UnitTester */
    protected $tester;

    public function test_get_limit_method(): void
    {
        $handler = new SessionLimiterHandler();
        $handler->setLimiter(new SimpleSessionLimiter(42));

        $this->assertSame(42, $handler->getLimit());
    }

    public function limitProvider(): array
    {
        return [
            '0 is the new illimited color...' => [0, true],
            'With limit 42' => [42, false]
        ];
    }

    /** @dataProvider limitProvider */
    public function test_is_illimited_method($limit, $expected): void
    {
        $handler = new SessionLimiterHandler();
        $handler->setLimiter(new SimpleSessionLimiter($limit));

        $this->assertSame($expected, $handler->isIllimited());
    }

    public function dataForLimitReachedProvider(): array
    {
        return [
            'With limit 0' => [0, false],
            'With limit 42' => [42, true],
            'With limit 150' => [150, false]
        ];
    }

    /** @dataProvider dataForLimitReachedProvider */
    public function test_is_limit_reached_method($limit, $expected): void
    {
        foreach (\range(1, 100) as $item) {
            $activeSessions[] = $this->tester->createTestActiveSession();
        }
        $handler = new SessionLimiterHandler();
        $handler->setLimiter(new SimpleSessionLimiter($limit));

        $result = $handler->isLimitReached($activeSessions);

        $this->assertSame($expected, $result);
    }
}
