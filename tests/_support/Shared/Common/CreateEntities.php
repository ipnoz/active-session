<?php

/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\ActiveSessionBundle\Tests\Shared\Common;

use Ipnoz\ActiveSessionBundle\Tests\TestsApp\Entity\TestActiveSession;
use Ipnoz\ActiveSessionBundle\Tests\TestsApp\Entity\TestUser;

/**
 * Trait to create easily entities.
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait CreateEntities
{
    public function createTestUser(): TestUser
    {
        return new TestUser();
    }

    public function createTestActiveSession(array $data = []): TestActiveSession
    {
        $default = [
            'user' => null,
            'ip' => '127.0.0.1',
            'user_agent' => 'userAgent',
            'created_at' => new \DateTime(),
            'deactivated_at' => null,
            'blocked_at' => null
        ];
        $data = \array_merge($default, $data);

        $activeSession = new TestActiveSession();
        $activeSession->setUser($data['user']);
        $activeSession->setIp($data['ip']);
        $activeSession->setUserAgent($data['user_agent']);
        $activeSession->setCreatedAt($data['created_at']);
        $activeSession->setDeactivatedAt($data['deactivated_at']);
        $activeSession->setBlockedAt($data['blocked_at']);

        return $activeSession;
    }
}
