<?php

/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\ActiveSessionBundle\Tests\Shared\Functional;

use Ipnoz\ActiveSessionBundle\Tests\Helper\Page\Page;
use Ipnoz\ActiveSessionBundle\Tests\TestsApp\Entity\TestUser;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\RememberMeToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait LoginTesterActions
{
    /**
     * Simulate authentication by creating the Authentication Token and
     * populate both cookie and session.
     */
    private function AuthenticateUser(UserInterface $user): void
    {
        $I = $this;
        $firewall = 'main';

        $token = new RememberMeToken($user, $firewall, 'secretOfTheFramework');

        /** @var TokenStorageInterface $tokenStorage */
        $tokenStorage = $I->grabService('security.token_storage');
        $tokenStorage->setToken($token);

        /** @var SessionInterface $session */
        $session = $I->grabService('session');
        $session->set('_security_'.$firewall, \serialize($token));
        $session->save();

        $I->setCookie($session->getName(), $session->getId());
    }

    public function amLogguedAsTestUser(TestUser $user, string $cookie = null): void
    {
        $this->AuthenticateUser($user);

        if (\is_string($cookie)) {
            $this->setCookie(Page::COOKIE_NAME, $cookie);
        }
    }
}
