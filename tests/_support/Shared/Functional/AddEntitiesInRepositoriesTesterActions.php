<?php

/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\ActiveSessionBundle\Tests\Shared\Functional;

use Ipnoz\ActiveSessionBundle\Tests\TestsApp\Entity\TestActiveSession;
use Ipnoz\ActiveSessionBundle\Tests\TestsApp\Entity\TestUser;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait AddEntitiesInRepositoriesTesterActions
{
    public function haveTestUserInRepository(): TestUser
    {
        $id = $this->haveInRepository($this->createTestUser());
        return $this->grabEntityFromRepository(TestUser::class, ['id' => $id]);
    }

    public function haveTestActiveSessionInRepository(array $data = []): TestActiveSession
    {
        $id = $this->haveInRepository($this->createTestActiveSession($data));
        return $this->grabEntityFromRepository(TestActiveSession::class, ['id' => $id]);
    }
}
