<?php

/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\ActiveSessionBundle\Tests\Shared\Functional;

use Ipnoz\ActiveSessionBundle\Tests\Helper\Page\Page;
use Ipnoz\ActiveSessionBundle\Tests\TestsApp\Entity\TestActiveSession;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait TesterActions
{
    public function seeNothingHappened(): void
    {
        $I = $this;
        $I->dontSeeCookie(Page::COOKIE_NAME);
        $I->dontSeeInRepository(TestActiveSession::class);
    }

    public function seeNewActiveSessionHasBeenCreatedForUser(): void
    {
        $I = $this;
        $I->seeCookie(Page::COOKIE_NAME);
        $I->seeInRepository(TestActiveSession::class, ['token' => $I->grabCookie(Page::COOKIE_NAME)]);
    }

    public function seeTheSessionBlockedPage(): void
    {
        $I = $this;
        $I->seeResponseCodeIs(403);
        $I->see('Oups ! 403 Active session limit reached');
    }
}
