<?php

/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\ActiveSessionBundle\Tests\TestsApp\Repository\QueryExpr;

use Ipnoz\ActiveSessionBundle\Repository\QueryExpr\AbstractQueryExpr;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class TestAbstractQueryExpr extends AbstractQueryExpr
{
}
