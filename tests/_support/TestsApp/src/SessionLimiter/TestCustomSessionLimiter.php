<?php

/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\ActiveSessionBundle\Tests\TestsApp\SessionLimiter;

use Ipnoz\ActiveSessionBundle\SessionLimiter\SessionLimiterInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class TestCustomSessionLimiter implements SessionLimiterInterface
{
    public function getLimit(): int
    {
        return 5;
    }
}
