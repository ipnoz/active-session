<?php

/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\ActiveSessionBundle\Tests\TestsApp\Controller;

use Ipnoz\ActiveSessionBundle\Entity\ActiveSessionManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class TestController extends Controller
{
    private $manager;

    public function __construct(ActiveSessionManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Route("/", name="")
     */
    public function indexAction(): Response
    {
        return new Response();
    }

    /**
     * @Route("/excluded-route-name", name="excluded_route_name")
     */
    public function excludedRouteNameAction(): Response
    {
        return new Response();
    }

    /**
     * @Route("/excluded-method", name="excluded_method")
     */
    public function excludedMethodAction(): Response
    {
        return new Response();
    }

    /**
     * @Route("/find-from-cookie", name="find_from_cookie")
     */
    public function findFromCookieAction(): Response
    {
        $activeSession = $this->manager->findFromCookie();
        return new Response($activeSession->getIp().' : '.$activeSession->getToken());
    }
}
