<?php

/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\ActiveSessionBundle\Tests\TestsApp\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ipnoz\ActiveSessionBundle\Entity\ActiveSession;

/**
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ipnoz\ActiveSessionBundle\Tests\TestsApp\Repository\TestActiveSessionRepository")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class TestActiveSession extends ActiveSession
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    public function getId(): ?int
    {
        return $this->id;
    }
}
