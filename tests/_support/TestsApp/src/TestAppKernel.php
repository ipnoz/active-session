<?php

/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\ActiveSessionBundle\Tests\TestsApp;

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class TestAppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            new \Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new \Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new \Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new \Symfony\Bundle\TwigBundle\TwigBundle(),
            new \Ipnoz\ActiveSessionBundle\IpnozActiveSessionBundle(),

            new \Ipnoz\ActiveSessionBundle\Tests\TestsApp\TestAppBundle()
        ];

        return $bundles;
    }

    public function getVarDir(): string
    {
        return \dirname(__DIR__).'/../../../var';
    }

    public function getLogDir(): string
    {
        return $this->getVarDir().'/logs';
    }

    public function getCacheDir(): string
    {
        return $this->getVarDir().'/cache/test/';
    }

    public function registerContainerConfiguration(LoaderInterface $loader): void
    {
        $excludeEnv = ['test', 'dev'];

        $loader->load(__DIR__.'/../config/config.yml');

        if (! \in_array($this->getEnvironment(), $excludeEnv)) {
            $loader->load(__DIR__.'/../config/config_'.$this->getEnvironment().'.yml');
        }
    }
}
