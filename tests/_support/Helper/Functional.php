<?php

/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\ActiveSessionBundle\Tests\Helper;

use Codeception\TestInterface;

/**
 * Here you can define custom actions
 * All public methods declared in helper class will be available in $I
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class Functional extends \Codeception\Module
{
    /*
     * Reconfigure with the default environment after each test, because the
     * _reconfigure method apply the configuration for the rest of the
     * Codeception process.
     */
    public function _after(TestInterface $test)
    {
        $this->getModule('Symfony')->_reconfigure(['environment' => 'test']);
    }
}
