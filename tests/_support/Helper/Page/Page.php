<?php

/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\ActiveSessionBundle\Tests\Helper\Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class Page
{
    const COOKIE_NAME = 'test-active-session';
}
