<?php

/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\ActiveSessionBundle\Tests\functional\Entity;

use Codeception\Module\Symfony;
use Ipnoz\ActiveSessionBundle\Entity\ActiveSessionManager;
use Ipnoz\ActiveSessionBundle\Model\ActiveSessionInterface;
use Ipnoz\ActiveSessionBundle\Tests\FunctionalTester;
use Ipnoz\ActiveSessionBundle\Tests\Helper\Page\Page;
use Ipnoz\ActiveSessionBundle\Tests\TestsApp\Entity\TestActiveSession;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ActiveSessionManagerCest
{
    /** @var ActiveSessionManager */
    private $manager;

    public function _before(FunctionalTester $I): void
    {
        $this->manager = $I->grabService(ActiveSessionManager::class);
    }

    public function it_test_that_the_new_method_throw_a_runtime_exception_without_a_http_request(FunctionalTester $I): void
    {
        $user = $I->haveTestUserInRepository();

        $I->expectThrowable(\RuntimeException::class, function () use ($user) {
            $this->manager->new($user);
        });
    }

    public function it_test_that_the_new_method_throw_an_invalid_argument_exception_without_a_valid_user(FunctionalTester $I): void
    {
        $I->haveTestUserInRepository();

        $I->expectThrowable(\InvalidArgumentException::class, function () {
            $this->manager->new(null);
        });
    }

    public function it_test_the_block_method_without_block_delay(FunctionalTester $I): void
    {
        $user = $I->haveTestUserInRepository();
        $activeSession = $I->haveTestActiveSessionInRepository(['user' => $user]);

        $this->manager->block($activeSession);

        $I->assertTrue($activeSession->isBlocked());
    }

    protected function withBlockDelay(Symfony $symfony): void
    {
        $symfony->unpersistService('doctrine.orm.entity_manager');
        $symfony->_reconfigure(['environment' => 'withBlockDelay']);
    }

    /** @prepare withBlockDelay */
    public function it_test_the_block_method_with_block_delay(FunctionalTester $I): void
    {
        $user = $I->haveTestUserInRepository();
        $activeSession = $I->haveTestActiveSessionInRepository(['user' => $user]);

        $this->manager->block($activeSession);

        $I->assertFalse($activeSession->isBlocked());
    }

    public function it_test_the_deactivate_olds_method(FunctionalTester $I): void
    {
        $user = $I->haveTestUserInRepository();
        $I->haveTestActiveSessionInRepository(['user' => $user, 'created_at' => new \DateTime('-31 days')]);

        $this->manager->deactivateOlds('-14 days');
        $I->clearEntityManager();
        /** @var ActiveSessionInterface[] $activeSessions */
        $activeSessions = $I->grabEntitiesFromRepository(TestActiveSession::class);

        $I->assertTrue($activeSessions[0]->isDeactivated());
    }

    public function it_test_the_find_from_cookie_method(FunctionalTester $I): void
    {
        $user = $I->haveTestUserInRepository();
        $activeSession = $I->haveTestActiveSessionInRepository(['user' => $user]);
        $I->amLogguedAsTestUser($user);
        $I->setCookie(Page::COOKIE_NAME, $activeSession->getToken());

        $I->amOnPage('/find-from-cookie');

        $I->see($activeSession->getIp().' : '.$activeSession->getToken());
    }

    /*
     * This test proves that when a new active session is created for a user,
     * the process set the cookie (it can be a redirect or added in the headers
     * or whatever). If not, it can produce unexpected behavior.
     */
    public function it_test_the_find_from_cookie_method_with_newly_created_active_session(FunctionalTester $I): void
    {
        $user = $I->haveTestUserInRepository();
        $I->amLogguedAsTestUser($user);

        $I->amOnPage('/find-from-cookie');

        $I->seeCookie(Page::COOKIE_NAME);
        $I->see($I->grabCookie(Page::COOKIE_NAME));
    }
}
