<?php

/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\ActiveSessionBundle\Tests\functional\Scenarios;

use Ipnoz\ActiveSessionBundle\Tests\FunctionalTester;
use Ipnoz\ActiveSessionBundle\Tests\TestsApp\Entity\TestActiveSession;

/**
 * Testing scenarios where the user active sessions have to be regularized
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class SessionRegulationScenariosCest
{
    public function it_test_the_regulatation_process_on_max_session_limit_not_reached(FunctionalTester $I): void
    {
        $user = $I->haveTestUserInRepository();
        $activeSession1 = $I->haveTestActiveSessionInRepository(['user' => $user]);
        $activeSession2 = $I->haveTestActiveSessionInRepository(['user' => $user, 'blocked_at' => new \DateTime('+1 day')]);
        $activeSession3 = $I->haveTestActiveSessionInRepository(['user' => $user]);

        $I->amLogguedAsTestUser($user, $activeSession3->getToken());
        $I->amOnPage('/');

        $I->seeResponseCodeIsSuccessful();
        // Check in database that the active session has been regulariezd
        $I->assertCount(3, $I->grabEntitiesFromRepository(TestActiveSession::class, ['blockedAt' => null]));
    }

    public function it_test_the_regulatation_process_when_the_max_session_limit_is_reached(FunctionalTester $I): void
    {
        $user = $I->haveTestUserInRepository();
        $activeSession1 = $I->haveTestActiveSessionInRepository(['user' => $user]);
        $activeSession2 = $I->haveTestActiveSessionInRepository(['user' => $user]);
        $activeSession3 = $I->haveTestActiveSessionInRepository(['user' => $user, 'blocked_at' => new \DateTime('+2 day')]);
        $activeSession4 = $I->haveTestActiveSessionInRepository(['user' => $user, 'blocked_at' => new \DateTime('+3 day')]);
        $activeSession5 = $I->haveTestActiveSessionInRepository(['user' => $user, 'blocked_at' => new \DateTime('+4 day')]);
        $activeSession6 = $I->haveTestActiveSessionInRepository(['user' => $user]);

        $I->amLogguedAsTestUser($user, $activeSession6->getToken());
        $I->amOnPage('/');

        $I->seeResponseCodeIsSuccessful();
        // Check in database that the active session has been regulariezd
        $unblockedSession = $I->grabEntitiesFromRepository(TestActiveSession::class, ['blockedAt' => null]);
        $I->assertSame([$activeSession1, $activeSession2, $activeSession3, $activeSession4, $activeSession6], $unblockedSession);
    }

    public function it_test_the_regulatation_process_when_the_max_session_limit_is_reached_with_too_much_blocked_sessions(FunctionalTester $I): void
    {
        $user = $I->haveTestUserInRepository();
        $activeSession1 = $I->haveTestActiveSessionInRepository(['user' => $user, 'blocked_at' => new \DateTime('+1 day')]);
        $activeSession2 = $I->haveTestActiveSessionInRepository(['user' => $user, 'blocked_at' => new \DateTime('+2 day')]);
        $activeSession3 = $I->haveTestActiveSessionInRepository(['user' => $user, 'blocked_at' => new \DateTime('+3 day')]);
        $activeSession4 = $I->haveTestActiveSessionInRepository(['user' => $user, 'blocked_at' => new \DateTime('+4 day')]);
        $activeSession5 = $I->haveTestActiveSessionInRepository(['user' => $user, 'blocked_at' => new \DateTime('+5 day')]);
        $activeSession6 = $I->haveTestActiveSessionInRepository(['user' => $user, 'blocked_at' => new \DateTime('+6 day')]);
        $activeSession7 = $I->haveTestActiveSessionInRepository(['user' => $user, 'blocked_at' => new \DateTime('+7 day')]);

        $I->amLogguedAsTestUser($user, $activeSession6->getToken());
        $I->amOnPage('/');

        $I->seeResponseCodeIsSuccessful();
        // Check in database that the active session has been regulariezd
        $unblockedSession = $I->grabEntitiesFromRepository(TestActiveSession::class, ['blockedAt' => null]);
        $I->assertSame([$activeSession1, $activeSession2, $activeSession3, $activeSession4, $activeSession5], $unblockedSession);
    }

    /*
     * If the max_sessions is under the total of authorized active session, none action is expected
     * max_session test parameter = 5
     * total of authorized session = 8
     */
    public function it_test_the_regulatation_process_when_the_max_session_limit_is_reached_with_unblocked_sessions(FunctionalTester $I): void
    {
        $user = $I->haveTestUserInRepository();
        $activeSessions[] = $I->haveTestActiveSessionInRepository(['user' => $user]);
        $activeSessions[] = $I->haveTestActiveSessionInRepository(['user' => $user]);
        $activeSessions[] = $I->haveTestActiveSessionInRepository(['user' => $user]);
        $activeSessions[] = $I->haveTestActiveSessionInRepository(['user' => $user]);
        $activeSessions[] = $I->haveTestActiveSessionInRepository(['user' => $user]);
        $activeSessions[] = $I->haveTestActiveSessionInRepository(['user' => $user]);
        $activeSessions[] = $I->haveTestActiveSessionInRepository(['user' => $user]);
        $activeSessions[] = $I->haveTestActiveSessionInRepository(['user' => $user]);

        $I->amLogguedAsTestUser($user, $activeSessions[5]->getToken());
        $I->amOnPage('/');

        $I->seeResponseCodeIsSuccessful();
        // Check in database that the active session has been regulated
        $result = $I->grabEntitiesFromRepository(TestActiveSession::class, ['blockedAt' => null]);
        $I->assertSame($activeSessions, $result);
    }
}
