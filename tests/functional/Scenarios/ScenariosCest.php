<?php

/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\ActiveSessionBundle\Tests\functional\Scenarios;

use Codeception\Example;
use Codeception\Module\Symfony;
use Ipnoz\ActiveSessionBundle\Tests\FunctionalTester;
use Ipnoz\ActiveSessionBundle\Tests\TestsApp\Entity\TestActiveSession;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ScenariosCest
{
    public function it_test_when_user_is_not_loggued(FunctionalTester $I): void
    {
        $I->haveTestUserInRepository();

        $I->amOnPage('/');

        $I->seeResponseCodeIsSuccessful();
        $I->seeNothingHappened();
    }

    public function it_test_when_user_is_loggued(FunctionalTester $I): void
    {
        $user = $I->haveTestUserInRepository();

        $I->amLogguedAsTestUser($user);
        $I->amOnPage('/');

        $I->seeResponseCodeIsSuccessful();
        $I->seeNewActiveSessionHasBeenCreatedForUser();
    }

    protected function excludedRoutesProvider(): array
    {
        return [
            ['route' => '/excluded-controller'],
            ['route' => '/excluded-method'],
            ['route' => '/excluded-route-name'],
        ];
    }

    /*
     * Unpersist the entityManager on Symfony reconfiguration to avoid error
     * with managed entities
     */
    protected function withExcluded(Symfony $symfony): void
    {
        $symfony->unpersistService('doctrine.orm.entity_manager');
        $symfony->_reconfigure(['environment' => 'withExcluded']);
    }

    /**
     * @prepare withExcluded
     * @dataProvider excludedRoutesProvider
     */
    public function it_test_when_user_is_on_an_excluded_page(FunctionalTester $I, Example $data): void
    {
        $I->amOnPage($data['route']);

        $I->seeResponseCodeIsSuccessful();
        $I->seeNothingHappened();
    }

    /**
     * @prepare withExcluded
     * @dataProvider excludedRoutesProvider
     */
    public function it_test_when_user_is_loggued_and_on_an_excluded_page(FunctionalTester $I, Example $data): void
    {
        $user = $I->haveTestUserInRepository();

        $I->amLogguedAsTestUser($user);
        $I->amOnPage($data['route']);

        $I->seeResponseCodeIsSuccessful();
        $I->seeNothingHappened();
    }

    protected function withMaxSessionZero(Symfony $symfony): void
    {
        $symfony->unpersistService('doctrine.orm.entity_manager');
        $symfony->_reconfigure(['environment' => 'maxSessionZero']);
    }

    /** @prepare withMaxSessionZero */
    public function it_test_when_user_is_loggued_and_the_limiter_has_no_limit(FunctionalTester $I): void
    {
        $user = $I->haveTestUserInRepository();

        $I->amLogguedAsTestUser($user);
        $I->amOnPage('/');

        $I->seeResponseCodeIsSuccessful();
        $I->seeNothingHappened();
    }

    protected function withLimiterClass(Symfony $symfony): void
    {
        $symfony->unpersistService('doctrine.orm.entity_manager');
        $symfony->_reconfigure(['environment' => 'limiterClass']);
    }

    /** @prepare withLimiterClass */
    public function it_test_when_user_is_loggued_and_a_limiter_class(FunctionalTester $I): void
    {
        $user = $I->haveTestUserInRepository();

        $I->amLogguedAsTestUser($user);
        $I->amOnPage('/');

        $I->seeResponseCodeIsSuccessful();
        $I->seeNewActiveSessionHasBeenCreatedForUser();
    }

    public function it_test_when_user_has_an_active_session(FunctionalTester $I): void
    {
        $user = $I->haveTestUserInRepository();
        $activeSession = $I->haveTestActiveSessionInRepository(['user' => $user]);

        $I->amLogguedAsTestUser($user, $activeSession->getToken());
        $I->amOnPage('/');

        $I->seeResponseCodeIsSuccessful();
        $I->assertCount(1, $I->grabEntitiesFromRepository(TestActiveSession::class));
    }

    public function it_test_when_user_has_a_deactivated_session(FunctionalTester $I): void
    {
        $user = $I->haveTestUserInRepository();
        $activeSession = $I->haveTestActiveSessionInRepository(['user' => $user, 'deactivated_at' => new \DateTime('-1 day')]);

        $I->amLogguedAsTestUser($user, $activeSession->getToken());
        $I->amOnPage('/');

        $I->seeResponseCodeIsSuccessful();
        // Check in database a new active session has been created
        $I->assertCount(2, $I->grabEntitiesFromRepository(TestActiveSession::class));
    }

    public function it_test_when_user_has_a_delayed_deactivated_session(FunctionalTester $I): void
    {
        $user = $I->haveTestUserInRepository();
        $activeSession = $I->haveTestActiveSessionInRepository(['user' => $user, 'deactivated_at' => new \DateTime('+1 day')]);

        $I->amLogguedAsTestUser($user, $activeSession->getToken());
        $I->amOnPage('/');

        $I->seeResponseCodeIsSuccessful();
        $I->assertCount(1, $I->grabEntitiesFromRepository(TestActiveSession::class));
    }

    public function it_test_when_user_has_a_blocked_session(FunctionalTester $I): void
    {
        $user = $I->haveTestUserInRepository();
        $activeSession = $I->haveTestActiveSessionInRepository(['user' => $user, 'blocked_at' => new \DateTime('-1 day')]);

        $I->amLogguedAsTestUser($user, $activeSession->getToken());
        $I->amOnPage('/');

        $I->seeTheSessionBlockedPage();
        $I->assertCount(1, $I->grabEntitiesFromRepository(TestActiveSession::class));
    }

    public function it_test_when_user_has_a_delayed_blocked_session(FunctionalTester $I): void
    {
        $user = $I->haveTestUserInRepository();
        $activeSession = $I->haveTestActiveSessionInRepository(['user' => $user, 'blocked_at' => new \DateTime('+1 day')]);

        $I->amLogguedAsTestUser($user, $activeSession->getToken());
        $I->amOnPage('/');

        $I->seeResponseCodeIsSuccessful();
        $I->assertCount(1, $I->grabEntitiesFromRepository(TestActiveSession::class));
    }

    public function it_test_when_user_has_a_blocked_and_deactivated_active_session(FunctionalTester $I): void
    {
        $user = $I->haveTestUserInRepository();
        $activeSession = $I->haveTestActiveSessionInRepository(['user' => $user, 'blocked_at' => new \DateTime('-1 day'), 'deactivated_at' => new \DateTime('-1 day')]);

        $I->amLogguedAsTestUser($user, $activeSession->getToken());
        $I->amOnPage('/');

        $I->seeResponseCodeIsSuccessful();
        // Check in database a new active session has been created
        $I->assertCount(2, $I->grabEntitiesFromRepository(TestActiveSession::class));
    }

    public function it_test_when_user_has_reached_max_session_limit(FunctionalTester $I): void
    {
        $user = $I->haveTestUserInRepository();
        $I->haveTestActiveSessionInRepository(['user' => $user]);
        $I->haveTestActiveSessionInRepository(['user' => $user]);
        $I->haveTestActiveSessionInRepository(['user' => $user]);
        $I->haveTestActiveSessionInRepository(['user' => $user]);
        $I->haveTestActiveSessionInRepository(['user' => $user]);

        $I->amLogguedAsTestUser($user);
        $I->amOnPage('/');

        $I->seeTheSessionBlockedPage();
        $I->seeNewActiveSessionHasBeenCreatedForUser();
        // Check in database a new active session has been created
        $I->assertCount(6, $I->grabEntitiesFromRepository(TestActiveSession::class));
    }

    protected function withBlockDelay(Symfony $symfony): void
    {
        $symfony->unpersistService('doctrine.orm.entity_manager');
        $symfony->_reconfigure(['environment' => 'withBlockDelay']);
    }

    /** @prepare withBlockDelay */
    public function it_test_when_user_has_reached_max_session_limit_and_with_a_block_delay(FunctionalTester $I): void
    {
        $user = $I->haveTestUserInRepository();
        $I->haveTestActiveSessionInRepository(['user' => $user]);
        $I->haveTestActiveSessionInRepository(['user' => $user]);
        $I->haveTestActiveSessionInRepository(['user' => $user]);
        $I->haveTestActiveSessionInRepository(['user' => $user]);
        $I->haveTestActiveSessionInRepository(['user' => $user]);

        $I->amLogguedAsTestUser($user);
        $I->amOnPage('/');

        $I->seeResponseCodeIsSuccessful();
        $I->seeNewActiveSessionHasBeenCreatedForUser();
        // Check in database a new active session has been created
        $I->assertCount(6, $I->grabEntitiesFromRepository(TestActiveSession::class));
    }

    public function it_test_that_the_max_session_limit_process_works_only_with_current_user(FunctionalTester $I): void
    {
        $currentUser = $I->haveTestUserInRepository();
        $otherUser = $I->haveTestUserInRepository();
        $I->haveTestActiveSessionInRepository(['user' => $currentUser]);
        $I->haveTestActiveSessionInRepository(['user' => $otherUser]);
        $I->haveTestActiveSessionInRepository(['user' => $otherUser]);
        $I->haveTestActiveSessionInRepository(['user' => $currentUser]);
        $I->haveTestActiveSessionInRepository(['user' => $otherUser]);
        $I->haveTestActiveSessionInRepository(['user' => $currentUser]);
        $I->haveTestActiveSessionInRepository(['user' => $currentUser]);
        $I->haveTestActiveSessionInRepository(['user' => $otherUser]);

        $I->amLogguedAsTestUser($currentUser);
        $I->amOnPage('/');

        $I->seeResponseCodeIsSuccessful();
        $I->seeNewActiveSessionHasBeenCreatedForUser();
        // Check in database a new active session has been created
        $I->assertCount(9, $I->grabEntitiesFromRepository(TestActiveSession::class));
        $I->assertCount(5, $I->grabEntitiesFromRepository(TestActiveSession::class, ['user' => $currentUser->getId()]));
    }

    public function it_test_that_the_max_session_limit_process_do_not_include_deactivated_sessions(FunctionalTester $I): void
    {
        $user = $I->haveTestUserInRepository();
        $I->haveTestActiveSessionInRepository(['user' => $user]);
        $I->haveTestActiveSessionInRepository(['user' => $user]);
        $I->haveTestActiveSessionInRepository(['user' => $user, 'deactivated_at' => new \DateTime('-1 day')]);
        $I->haveTestActiveSessionInRepository(['user' => $user, 'deactivated_at' => new \DateTime('-1 day')]);
        $I->haveTestActiveSessionInRepository(['user' => $user, 'deactivated_at' => new \DateTime('-1 day')]);
        $I->haveTestActiveSessionInRepository(['user' => $user, 'deactivated_at' => new \DateTime('-1 day')]);
        $I->haveTestActiveSessionInRepository(['user' => $user, 'deactivated_at' => new \DateTime('-1 day')]);
        $I->haveTestActiveSessionInRepository(['user' => $user]);
        $I->haveTestActiveSessionInRepository(['user' => $user]);

        $I->amLogguedAsTestUser($user);
        $I->amOnPage('/');

        $I->seeResponseCodeIsSuccessful();
        $I->seeNewActiveSessionHasBeenCreatedForUser();
        // Check in database a new active session has been created
        $I->assertCount(10, $I->grabEntitiesFromRepository(TestActiveSession::class));
    }

    public function it_test_that_the_max_session_limit_process_include_sessions_which_will_be_deactivated(FunctionalTester $I): void
    {
        $user = $I->haveTestUserInRepository();
        $I->haveTestActiveSessionInRepository(['user' => $user]);
        $I->haveTestActiveSessionInRepository(['user' => $user]);
        $I->haveTestActiveSessionInRepository(['user' => $user, 'deactivated_at' => new \DateTime('+1 day')]);
        $I->haveTestActiveSessionInRepository(['user' => $user]);
        $I->haveTestActiveSessionInRepository(['user' => $user]);

        $I->amLogguedAsTestUser($user);
        $I->amOnPage('/');

        $I->seeTheSessionBlockedPage();
        $I->seeNewActiveSessionHasBeenCreatedForUser();
        // Check in database a new active session has been created
        $I->assertCount(6, $I->grabEntitiesFromRepository(TestActiveSession::class));
    }

    public function it_test_that_the_max_session_limit_process_do_not_include_blocked_sessions(FunctionalTester $I): void
    {
        $user = $I->haveTestUserInRepository();
        $I->haveTestActiveSessionInRepository(['user' => $user]);
        $I->haveTestActiveSessionInRepository(['user' => $user]);
        $I->haveTestActiveSessionInRepository(['user' => $user, 'blocked_at' => new \DateTime('-5 day')]);
        $I->haveTestActiveSessionInRepository(['user' => $user, 'blocked_at' => new \DateTime('-4 day')]);
        $I->haveTestActiveSessionInRepository(['user' => $user, 'blocked_at' => new \DateTime('-3 day')]);
        $I->haveTestActiveSessionInRepository(['user' => $user, 'blocked_at' => new \DateTime('-2 day')]);
        $I->haveTestActiveSessionInRepository(['user' => $user, 'blocked_at' => new \DateTime('-1 day')]);
        $I->haveTestActiveSessionInRepository(['user' => $user]);
        $I->haveTestActiveSessionInRepository(['user' => $user]);

        $I->amLogguedAsTestUser($user);
        $I->amOnPage('/');

        $I->seeResponseCodeIsSuccessful();
        $I->seeNewActiveSessionHasBeenCreatedForUser();
        // Check in database a new active session has been created
        $I->assertCount(10, $I->grabEntitiesFromRepository(TestActiveSession::class));
    }
}
