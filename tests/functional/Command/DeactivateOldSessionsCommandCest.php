<?php

/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\ActiveSessionBundle\Tests\functional\Command;

use Ipnoz\ActiveSessionBundle\Model\ActiveSessionInterface;
use Ipnoz\ActiveSessionBundle\Tests\FunctionalTester;
use Ipnoz\ActiveSessionBundle\Tests\TestsApp\Entity\TestActiveSession;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class DeactivateOldSessionsCommandCest
{
    public function it_test_the_deactivate_olds_command(FunctionalTester $I): void
    {
        $user = $I->haveTestUserInRepository();
        $I->haveTestActiveSessionInRepository(['user' => $user, 'created_at' => new \DateTime('-31 days')]);

        $I->runSymfonyConsoleCommand('ipnoz:active-session:deactivate-olds');
        $I->clearEntityManager();
        /** @var ActiveSessionInterface[] $activeSessions */
        $activeSessions = $I->grabEntitiesFromRepository(TestActiveSession::class);

        $I->assertTrue($activeSessions[0]->isDeactivated());
    }
}
