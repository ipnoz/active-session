<?php

/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\ActiveSessionBundle\Tests\functional\Repository\QueryExpr;

use Doctrine\ORM\Query\Parameter;
use Ipnoz\ActiveSessionBundle\Tests\FunctionalTester;
use Ipnoz\ActiveSessionBundle\Tests\TestsApp\Entity\TestActiveSession;
use Ipnoz\ActiveSessionBundle\Tests\TestsApp\Repository\QueryExpr\TestAbstractQueryExpr;
use Ipnoz\ActiveSessionBundle\Tests\TestsApp\Repository\TestActiveSessionRepository;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class AbstractQueryExprCest
{
    /** @var TestActiveSessionRepository */
    private $repository;

    public function _before(FunctionalTester $I): void
    {
        $this->repository = $I->grabService('doctrine')->getRepository(TestActiveSession::class);
    }

    public function it_test_the_set_parameter_now_method(FunctionalTester $I): void
    {
        $qb = $this->repository->createQueryBuilder('a');
        $qbe = new TestAbstractQueryExpr($qb);

        $qb->where(
            $qb->expr()->gte('a.deactivatedAt', ':now')
        );
        $qbe->setParameterNow();
        /** @var Parameter[] $parameters */
        $parameters = $qb->getParameters();

        $I->assertCount(1, $parameters);
        $I->assertSame('now', $parameters[0]->getName());
    }

    public function it_check_that_the_set_parameter_now_method_can_be_called_multiple_time_without_any_error(FunctionalTester $I): void
    {
        $qb = $this->repository->createQueryBuilder('a');
        $qbe = new TestAbstractQueryExpr($qb);

        $qb->where(
            $qb->expr()->gte('a.deactivatedAt', ':now')
        );
        $qbe->setParameterNow();
        $qbe->setParameterNow();
        $qbe->setParameterNow();

        $I->assertCount(1, $qb->getParameters());
    }
}
