<?php

/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\ActiveSessionBundle\Tests\functional\Repository;

use Ipnoz\ActiveSessionBundle\Tests\FunctionalTester;
use Ipnoz\ActiveSessionBundle\Tests\TestsApp\Entity\TestActiveSession;
use Ipnoz\ActiveSessionBundle\Tests\TestsApp\Repository\TestActiveSessionRepository;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ActiveSessionRepositoryCest
{
    /** @var TestActiveSessionRepository */
    private $repository;

    public function _before(FunctionalTester $I): void
    {
        $this->repository = $I->grabService('doctrine')->getRepository(TestActiveSession::class);
    }

    public function it_test_find_actives_method(FunctionalTester $I): void
    {
        $user = $I->haveTestUserInRepository();
        $activeSession1 = $I->haveTestActiveSessionInRepository(['user' => $user]);
        $activeSession2 = $I->haveTestActiveSessionInRepository(['user' => $user, 'deactivated_at' => new \DateTime('-1 day')]);
        $activeSession3 = $I->haveTestActiveSessionInRepository(['user' => $user, 'deactivated_at' => new \DateTime('+1 day')]);

        $result = $this->repository->findActives();

        $I->assertSame([$activeSession1, $activeSession3], $result);
    }

    public function it_test_find_actives_method_with_user_arg(FunctionalTester $I): void
    {
        $user1 = $I->haveTestUserInRepository();
        $user2 = $I->haveTestUserInRepository();
        $activeSession1 = $I->haveTestActiveSessionInRepository(['user' => $user1]);
        $activeSession2 = $I->haveTestActiveSessionInRepository(['user' => $user1, 'deactivated_at' => new \DateTime('-1 day')]);
        $activeSession3 = $I->haveTestActiveSessionInRepository(['user' => $user1, 'deactivated_at' => new \DateTime('+1 day')]);
        $activeSession4 = $I->haveTestActiveSessionInRepository(['user' => $user2]);
        $activeSession5 = $I->haveTestActiveSessionInRepository(['user' => $user2, 'deactivated_at' => new \DateTime('-1 day')]);
        $activeSession6 = $I->haveTestActiveSessionInRepository(['user' => $user2, 'deactivated_at' => new \DateTime('+1 day')]);

        $result = $this->repository->findActives($user2->getId());

        $I->assertSame([$activeSession4, $activeSession6], $result);
    }

    public function it_test_find_active_and_not_blocked_method(FunctionalTester $I): void
    {
        $user = $I->haveTestUserInRepository();
        $activeSession1 = $I->haveTestActiveSessionInRepository(['user' => $user]);
        $activeSession2 = $I->haveTestActiveSessionInRepository(['user' => $user, 'deactivated_at' => new \DateTime('-1 day')]);
        $activeSession3 = $I->haveTestActiveSessionInRepository(['user' => $user, 'deactivated_at' => new \DateTime('+1 day')]);
        $activeSession4 = $I->haveTestActiveSessionInRepository(['user' => $user, 'blocked_at' => new \DateTime('-1 day')]);
        $activeSession5 = $I->haveTestActiveSessionInRepository(['user' => $user, 'blocked_at' => new \DateTime('+1 day')]);
        $activeSession6 = $I->haveTestActiveSessionInRepository(['user' => $user, 'deactivated_at' => new \DateTime('-1 day'), 'blocked_at' => new \DateTime('-1 day')]);
        $activeSession7 = $I->haveTestActiveSessionInRepository(['user' => $user, 'deactivated_at' => new \DateTime('+1 day'), 'blocked_at' => new \DateTime('+1 day')]);
        $activeSession8 = $I->haveTestActiveSessionInRepository(['user' => $user, 'deactivated_at' => new \DateTime('-1 day'), 'blocked_at' => new \DateTime('+1 day')]);
        $activeSession9 = $I->haveTestActiveSessionInRepository(['user' => $user, 'deactivated_at' => new \DateTime('+1 day'), 'blocked_at' => new \DateTime('-1 day')]);

        $result = $this->repository->findActiveAndNotBlocked();

        $I->assertSame([$activeSession1, $activeSession3, $activeSession5, $activeSession7], $result);
    }

    public function it_test_find_active_and_not_blocked_method_with_user_arg(FunctionalTester $I): void
    {
        $user1 = $I->haveTestUserInRepository();
        $user2 = $I->haveTestUserInRepository();
        $activeSession1 = $I->haveTestActiveSessionInRepository(['user' => $user1]);
        $activeSession2 = $I->haveTestActiveSessionInRepository(['user' => $user1, 'deactivated_at' => new \DateTime('-1 day')]);
        $activeSession3 = $activeSession3 = $I->haveTestActiveSessionInRepository(['user' => $user2, 'deactivated_at' => new \DateTime('+1 day')]);
        $activeSession4 = $I->haveTestActiveSessionInRepository(['user' => $user1, 'blocked_at' => new \DateTime('-1 day')]);
        $activeSession5 = $I->haveTestActiveSessionInRepository(['user' => $user1, 'blocked_at' => new \DateTime('+1 day')]);
        $activeSession6 = $I->haveTestActiveSessionInRepository(['user' => $user1, 'deactivated_at' => new \DateTime('-1 day'), 'blocked_at' => new \DateTime('-1 day')]);
        $activeSession7 = $I->haveTestActiveSessionInRepository(['user' => $user2, 'deactivated_at' => new \DateTime('+1 day'), 'blocked_at' => new \DateTime('+1 day')]);
        $activeSession8 = $I->haveTestActiveSessionInRepository(['user' => $user1, 'deactivated_at' => new \DateTime('-1 day'), 'blocked_at' => new \DateTime('+1 day')]);
        $activeSession9 = $I->haveTestActiveSessionInRepository(['user' => $user1, 'deactivated_at' => new \DateTime('+1 day'), 'blocked_at' => new \DateTime('-1 day')]);

        $result = $this->repository->findActiveAndNotBlocked($user2->getId());

        $I->assertSame([$activeSession3, $activeSession7], $result);
    }

    public function it_test_find_blocked_in_last_days_method(FunctionalTester $I): void
    {
        $user = $I->haveTestUserInRepository();
        $activeSession1 = $I->haveTestActiveSessionInRepository(['user' => $user, 'blocked_at' => new \DateTime('+3 day')]);
        $activeSession2 = $I->haveTestActiveSessionInRepository(['user' => $user]);
        $activeSession3 = $I->haveTestActiveSessionInRepository(['user' => $user, 'blocked_at' => new \DateTime('-1 day')]);
        $activeSession4 = $I->haveTestActiveSessionInRepository(['user' => $user, 'blocked_at' => new \DateTime('-7 day')]);
        $activeSession5 = $I->haveTestActiveSessionInRepository(['user' => $user, 'blocked_at' => new \DateTime('-10 day')]);
        $activeSession6 = $I->haveTestActiveSessionInRepository(['user' => $user, 'blocked_at' => new \DateTime('-17 day')]);

        $result = $this->repository->findBlockedInLastDays(7);

        $I->assertSame([$activeSession1, $activeSession3, $activeSession4], $result);
    }

    /*
     * *********************************************************************** *
     *                                Commands:
     * *********************************************************************** *
     */

    private function deactivateOldsProvider(): array
    {
        return [
            ['created' => '-41 days', 'deactivated' => null, 'blocked' => null, 'expectDeactivated' => true, 'expectBlocked' => false],
            ['created' => '-40 days', 'deactivated' => '+5 days', 'blocked' => null, 'expectDeactivated' => true, 'expectBlocked' => false],
            ['created' => '-39 days', 'deactivated' => null, 'blocked' => '+5 days', 'expectDeactivated' => true, 'expectBlocked' => false],
            ['created' => '-38 days', 'deactivated' => '+5 days', 'blocked' => '+5 days', 'expectDeactivated' => true, 'expectBlocked' => false],
            ['created' => '-37 days', 'deactivated' => '-5 days', 'blocked' => null, 'expectDeactivated' => true, 'expectBlocked' => false],
            ['created' => '-36 days', 'deactivated' => null, 'blocked' => '-5 days', 'expectDeactivated' => false, 'expectBlocked' => true],
            ['created' => '-35 days', 'deactivated' => '-5 days', 'blocked' => '-5 days', 'expectDeactivated' => true, 'expectBlocked' => true],
            ['created' => '-34 days', 'deactivated' => '+5 days', 'blocked' => '-5 days', 'expectDeactivated' => false, 'expectBlocked' => true],
            ['created' => '-33 days', 'deactivated' => '-5 days', 'blocked' => '+5 days', 'expectDeactivated' => true, 'expectBlocked' => false],
            ['created' => '-13 days', 'deactivated' => null, 'blocked' => null, 'expectDeactivated' => false, 'expectBlocked' => false],
            ['created' => '-12 days', 'deactivated' => '+5 days', 'blocked' => null, 'expectDeactivated' => false, 'expectBlocked' => false],
            ['created' => '-11 days', 'deactivated' => null, 'blocked' => '+5 days', 'expectDeactivated' => false, 'expectBlocked' => false],
            ['created' => '-10 days', 'deactivated' => '+5 days', 'blocked' => '+5 days', 'expectDeactivated' => false, 'expectBlocked' => false],
            ['created' => '-9 days', 'deactivated' => '-5 days', 'blocked' => null, 'expectDeactivated' => true, 'expectBlocked' => false],
            ['created' => '-8 days', 'deactivated' => null, 'blocked' => '-5 days', 'expectDeactivated' => false, 'expectBlocked' => true],
            ['created' => '-7 days', 'deactivated' => '-5 days', 'blocked' => '-5 days', 'expectDeactivated' => true, 'expectBlocked' => true],
            ['created' => '-6 days', 'deactivated' => '+5 days', 'blocked' => '-5 days', 'expectDeactivated' => false, 'expectBlocked' => true],
            ['created' => '-1 hour', 'deactivated' => '-5 days', 'blocked' => '+5 days', 'expectDeactivated' => true, 'expectBlocked' => false],
        ];
    }

    public function it_test_the_deactivate_olds_method(FunctionalTester $I): void
    {
        $datas = $this->deactivateOldsProvider();

        // Given
        $user = $I->haveTestUserInRepository();
        foreach ($datas as $data) {
            $params = ['user' => $user, 'created_at' => new \DateTime($data['created'])];
            if (! \is_null($data['deactivated'])) {
                $params['deactivated_at'] = new \DateTime($data['deactivated']);
            }
            if (! \is_null($data['blocked'])) {
                $params['blocked_at'] = new \DateTime($data['blocked']);
            }
            $I->haveTestActiveSessionInRepository($params);
        }

        // When (I deactivate ActiveSession created after than...)
        $this->repository->deactivateOlds('-14 days');
        $I->clearEntityManager();
        /** @var TestActiveSession[] $activeSessions */
        $activeSessions = $I->grabEntitiesFromRepository(TestActiveSession::class);

        // Then (test all active sessions)
        $i = 0;
        foreach ($datas as $data) {
            $I->assertSame($data['expectDeactivated'], $activeSessions[$i]->isDeactivated(), 'expectDeactivated for TestActiveSession Created at: '.$data['created']);
            $I->assertSame($data['expectBlocked'], $activeSessions[$i]->isBlocked(), 'expectBlocked for TestActiveSession Created at: '.$data['created']);
            ++$i;
        }
    }
}
