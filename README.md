
# Active-Session-Bundle

### This Symfony bundle provides tools to control and limit user sessions

## Requirements

    php >= 7.1
    Symfony 3.4

## Installation

### 1. Require the package
    
    composer require ipnoz/active-session-bundle

### 2. Create ActiveSession entity

    use Ipnoz\ActiveSessionBundle\Entity\ActiveSession;

    /**
     * @ORM\Table()
     * @ORM\Entity(repositoryClass="App\Repository\ActiveSessionRepository")
     */
    class ActiveSession extends BaseActiveSession
    {
         /**
          * @var integer
          *
          * @ORM\Column(type="integer")
          * @ORM\Id
          * @ORM\GeneratedValue(strategy="AUTO")
          */
          private $id;
        
          public function getId(): ?int
          {
               return $this->id;
          }
    }

### 3. And create repository

    use Ipnoz\ActiveSessionBundle\Repository\ActiveSessionRepository as BaseActiveSessionRepository;

    class ActiveSessionRepository extends BaseActiveSessionRepository
    {
    }

### 4. Set configuration

    ipnoz_active_session:

        # Set the maximum of active sessions one user can cumulate
        max_session:          5 # Default
    
        # Add a delay in day before block a session wich reached the max session limit
        block_delay:          0 # Default
    
        # The class name of the app user
        user_class:           App\Entity\User # Required
    
        # The class name of the active session
        active_session_class: App\Entity\ActiveSession # Required

        cookie_name:          ipnoz-active-session # Default
    
        # List of route name or controller name to exclude of the control
        excluded:             []

        command:
            deactivate_olds_in_days: 14 # Default

### 5. Enable the bundle in app/Kernel.php

    new \Ipnoz\ActiveSessionBundle\IpnozActiveSessionBundle()

### 6. Update the database so bundle can persist entities using Doctrine

    bin/console doctrine:schema:update --force

## Events
    ipnoz_active_session.event.access_blocked
    ipnoz_active_session.event.session_created
    ipnoz_active_session.event.limit_reached

## Console
###### path

    tests/_support/TestsApp/bin/console

###### Deactivate old active sessions

    tests/_support/TestsApp/bin/console ipnoz:active-session:deactivate-olds

## Tests setup

    vendor/bin/codecept build

    tests/_support/TestsApp/bin/console doctrine:database:create
    tests/_support/TestsApp/bin/console doctrine:schema:update --complete --force

## Run tests

    vendor/bin/codecept run