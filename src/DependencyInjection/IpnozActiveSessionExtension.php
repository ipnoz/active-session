<?php

/*
 * This file is part of the IpnozActiveSessionBundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\ActiveSessionBundle\DependencyInjection;

use Ipnoz\ActiveSessionBundle\SessionLimiter\SessionLimiterHandler;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\DependencyInjection\Reference;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class IpnozActiveSessionExtension extends Extension
{
    /**
     * @inheritDoc
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $xmlFileLoader = new XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $xmlFileLoader->load('services.xml');

        // Setup bundle configuration in the container
        $configuration = $this->getConfiguration($configs, $container);
        $config = $this->processConfiguration($configuration, $configs);

        $this->validateMaxSessionParameter($config);

        $container->setParameter(Configuration::ROOT_NAME.'.max_session', $config['max_session']);
        $container->setParameter(Configuration::ROOT_NAME.'.block_delay', $config['block_delay']);
        $container->setParameter(Configuration::ROOT_NAME.'.user_class', $config['user_class']);
        $container->setParameter(Configuration::ROOT_NAME.'.active_session_class', $config['active_session_class']);
        $container->setParameter(Configuration::ROOT_NAME.'.cookie_name', $config['cookie_name']);
        $container->setParameter(Configuration::ROOT_NAME.'.excluded', $config['excluded']);
        $container->setParameter(Configuration::ROOT_NAME.'.command.deactivate_olds_in_days', $config['command']['deactivate_olds_in_days']);

        // Setup the limiter alias: if the config value is integer, use the default ConfigLimiter
        if (\is_int($config['max_session'])) {
            $id = Configuration::ROOT_NAME.'.session_limiter.default';
        } else {
            $id = $config['max_session'];
        }
        $container->setAlias(Configuration::ROOT_NAME.'.session_limiter', $id);

        // Entity manager factory definition
        $ormEntityManagerDefinition = $container->getDefinition(Configuration::ROOT_NAME.'.entity_manager');
        if (\method_exists($ormEntityManagerDefinition, 'setFactory')) {
            $ormEntityManagerDefinition->setFactory([new Reference('doctrine'), 'getManager']);
        } else {
            $ormEntityManagerDefinition->setFactoryService('doctrine');
            $ormEntityManagerDefinition->setFactoryMethod('getManager');
        }

        // Inject the limiter service
        $limiter = $container->getDefinition(SessionLimiterHandler::class);
        $limiter->addMethodCall('setLimiter', [new Reference($id)]);
    }

    private function validateMaxSessionParameter($config): void
    {
        if (\is_string($config['max_session'])) {
            if (! \class_exists($config['max_session'], true)) {
                throw new \InvalidArgumentException('Class "'.$config['max_session'].'" does not exists');
            }
        } elseif (! \is_int($config['max_session'])) {
            throw new \InvalidArgumentException('Invalid type for path "'.Configuration::ROOT_NAME.'.max_session". Expected int, but got '.\gettype($config['max_session']).'.');
        }
    }
}
