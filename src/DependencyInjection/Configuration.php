<?php

/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\ActiveSessionBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class Configuration implements ConfigurationInterface
{
    const ROOT_NAME = 'ipnoz_active_session';

    /**
     * @inheritDoc
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder(self::ROOT_NAME);
        $rootNode = $treeBuilder->getRootNode($treeBuilder);

        $rootNode
            ->addDefaultsIfNotSet()
            ->children()
                ->scalarNode('max_session')->defaultValue(5)->cannotBeEmpty()->info('Set the maximum of active sessions one user can cumulate')->end()
                ->integerNode('block_delay')->defaultValue(0)->info('Add a delay in day before block a session wich reached the max session limit')->end()
                ->scalarNode('user_class')->cannotBeEmpty()->isRequired()->info('The class name of the app user')->end()
                ->scalarNode('active_session_class')->cannotBeEmpty()->isRequired()->info('The class name of the active session')->end()
                ->scalarNode('cookie_name')->defaultValue('ipnoz-active-session')->cannotBeEmpty()->end()
                ->arrayNode('excluded')
                    ->info('List of route name or controller to exclude of the control')
                    ->prototype('scalar')->end()
                ->end()
                ->arrayNode('command')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->integerNode('deactivate_olds_in_days')->defaultValue(14)->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
