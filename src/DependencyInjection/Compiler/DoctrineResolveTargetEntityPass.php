<?php

/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\ActiveSessionBundle\DependencyInjection\Compiler;

use Doctrine\ORM\Version;
use Ipnoz\ActiveSessionBundle\DependencyInjection\Configuration;
use Ipnoz\ActiveSessionBundle\Entity\ActiveSession;
use Ipnoz\ActiveSessionBundle\Entity\User;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class DoctrineResolveTargetEntityPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        $definition = $container->findDefinition('doctrine.orm.listeners.resolve_target_entity');

        $definition->addMethodCall('addResolveTargetEntity', [
            User::class,
            $container->getParameter(Configuration::ROOT_NAME.'.user_class'),
            []
        ]);

        $definition->addMethodCall('addResolveTargetEntity', [
            ActiveSession::class,
            $container->getParameter(Configuration::ROOT_NAME.'.active_session_class'),
            []
        ]);

        if (version_compare(Version::VERSION, '2.5.0-DEV') < 0) {
            $definition->addTag('doctrine.event_listener', ['event' => 'loadClassMetadata']);
        } else {
            $definition->addTag('doctrine.event_subscriber', ['connection' => 'default']);
        }
    }
}
