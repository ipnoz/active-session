<?php

/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\ActiveSessionBundle\Event;

use Ipnoz\ActiveSessionBundle\Model\ActiveSessionInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class ActiveSessionHasReachedLimitEvent extends Event
{
    const NAME = 'ipnoz_active_session.event.limit_reached';

    public $activeSession;

    public function __construct(ActiveSessionInterface $activeSession)
    {
        $this->activeSession = $activeSession;
    }
}
