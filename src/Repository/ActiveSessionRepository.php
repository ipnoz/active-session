<?php

/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Ipnoz\ActiveSessionBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Ipnoz\ActiveSessionBundle\Model\ActiveSessionInterface;
use Ipnoz\ActiveSessionBundle\Repository\QueryExpr\ActiveSessionQueryExpr;

/**
 * @method ActiveSessionInterface|null find($id, $lockMode = null, $lockVersion = null)
 * @method ActiveSessionInterface|null findOneBy(array $criteria, array $orderBy = null)
 * @method ActiveSessionInterface[]    findAll()
 * @method ActiveSessionInterface[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ActiveSessionRepository extends EntityRepository
{
    /**
     * @return ActiveSessionInterface[]
     */
    public function findActives(int $userId = null): array
    {
        $qb = $this->createQueryBuilder('a');
        $qbe = new ActiveSessionQueryExpr($qb);

        $qb
            ->select('a')
            ->where($qbe->actives())
            ->orderBy('a.createdAt', 'ASC');

        if (! \is_null($userId)) {
            $qb->andWhere('a.user = :user')->setParameter(':user', $userId);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @return ActiveSessionInterface[]
     */
    public function findActiveAndNotBlocked(int $userId = null): array
    {
        $qb = $this->createQueryBuilder('a');
        $qbe = new ActiveSessionQueryExpr($qb);

        $qb
            ->select('a')
            ->where($qbe->actives())
            ->andWhere($qbe->notBlocked())
            ->orderBy('a.createdAt', 'ASC');

        if (! \is_null($userId)) {
            $qb->andWhere('a.user = :user')->setParameter(':user', $userId);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @return ActiveSessionInterface[]
     */
    public function findBlockedInLastDays(int $days): array
    {
        $qb = $this->createQueryBuilder('a');
        $qb
            ->select('a')
            ->where('a.blockedAt >= :date_from')
                ->setParameter(':date_from', new \DateTime('-'.$days.' days'));

        return $qb->getQuery()->getResult();
    }

    /*
     * *********************************************************************** *
     *                                Commands:
     * *********************************************************************** *
     */

    public function deactivateOlds(string $datetime)
    {
        $limit = new \DateTime($datetime);

        $qb = $this->createQueryBuilder('a');
        $qbe = new ActiveSessionQueryExpr($qb);

        $qb
            ->update()
            ->set('a.deactivatedAt', '?1')
                ->setParameter(1,  (new \DateTime('now -2 seconds'))->format('Y-m-d H:i:s'))
            ->where('a.createdAt < ?2')
                ->setParameter(2, $limit->format('Y-m-d H:i:s'))
            ->andWhere($qbe->actives())
            ->andWhere($qbe->notBlocked())
        ;

        return $qb->getQuery()->execute();
    }

    public function flush(): void
    {
        $this->_em->flush();
    }
}
