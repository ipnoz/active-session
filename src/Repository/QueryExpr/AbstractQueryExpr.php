<?php

/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Ipnoz\ActiveSessionBundle\Repository\QueryExpr;

use Doctrine\ORM\QueryBuilder;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
abstract class AbstractQueryExpr
{
    protected $qb;
    protected $alias;

    public function __construct(QueryBuilder $queryBuilder)
    {
        $this->qb = $queryBuilder;

        $aliases = $queryBuilder->getRootAliases();
        $this->alias = $aliases[0];
    }

    /**
     * Set the parameter :now once
     */
    public function setParameterNow(): void
    {
        if (! \is_null($this->qb->getParameter('now'))) {
            return;
        }
        $this->qb->setParameter(':now', (new \DateTime())->format('Y-m-d H:i:s'));
    }
}
