<?php
/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Ipnoz\ActiveSessionBundle\Repository\QueryExpr;

use Doctrine\ORM\Query\Expr\Orx;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ActiveSessionQueryExpr extends AbstractQueryExpr
{
    public function actives(): Orx
    {
        $this->setParameterNow();

        return $this->qb->expr()->orX(
           $this->qb->expr()->isNull($this->alias.'.deactivatedAt'),
           $this->qb->expr()->gte($this->alias.'.deactivatedAt', ':now')
       );
    }

    public function notBlocked(): Orx
    {
        $this->setParameterNow();

        return $this->qb->expr()->orX(
            $this->qb->expr()->isNull($this->alias.'.blockedAt'),
            $this->qb->expr()->gte($this->alias.'.blockedAt', ':now')
        );
    }
}
