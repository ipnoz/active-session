<?php

/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\ActiveSessionBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Ipnoz\ActiveSessionBundle\Entity\ActiveSession;
use Ipnoz\ActiveSessionBundle\Entity\ActiveSessionManager;
use Ipnoz\ActiveSessionBundle\Event\AccessBlockedEvent;
use Ipnoz\ActiveSessionBundle\Event\ActiveSessionHasReachedLimitEvent;
use Ipnoz\ActiveSessionBundle\Model\ActiveSessionInterface;
use Ipnoz\ActiveSessionBundle\SessionLimiter\SessionLimiterHandler;
use Ipnoz\ActiveSessionBundle\Model\UserInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;
use Twig\Environment;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class SessionAuthorizeService
{
    private $activeSessionManager;
    private $sessionLimiter;
    private $security;
    private $requestStack;
    private $twig;
    private $eventDispatcher;
    private $userClass;
    private $cookieName;
    private $excluded;
    private $repository;

    private $tokenForTheCookie;

    public function __construct(
        ActiveSessionManager $activeSessionManager,
        SessionLimiterHandler $sessionLimiterManager,
        EntityManagerInterface $em,
        Security $security,
        RequestStack $requestStack,
        Environment $twig,
        EventDispatcherInterface $eventDispatcher,
        string $userClass,
        string $activeSessionClass,
        string $cookieName,
        array $excluded
    ) {
        $this->activeSessionManager = $activeSessionManager;
        $this->sessionLimiter = $sessionLimiterManager;
        $this->security = $security;
        $this->requestStack = $requestStack;
        $this->twig = $twig;
        $this->eventDispatcher = $eventDispatcher;
        $this->userClass = $userClass;
        $this->cookieName = $cookieName;
        $this->excluded = $excluded;
        $this->repository = $em->getRepository($activeSessionClass);
    }

    public function checkRequest($event): void
    {
        if (\method_exists($event, 'isMasterRequest') && ! $event->isMasterRequest()) {
            return;
        }

        if (\method_exists($event, 'isMainRequest') && ! $event->isMainRequest()) {
            return;
        }

        $request = $event->getRequest();

        if ($this->routeOrControllerIsExcluded($request)) {
            return;
        }

        if ($this->sessionLimiter->isIllimited()) {
            return;
        }

        $user = $this->security->getUser();

        if (\is_null($user) || \get_class($user) !== $this->userClass) {
            return;
        }

        /** @var UserInterface $user */
        /** @var ActiveSession[] $activeSessions */
        $activeSessions = $this->repository->findActives($user->getId());

        // Regularize sessions before authorize
        if ($this->sessionLimiter->regularization($activeSessions)) {
            $this->repository->flush();
        }

        // The user has an active session cookie, check it's validity
        if ($tokenFromCookie = $request->cookies->get($this->cookieName)) {

            foreach ($activeSessions as $activeSession) {
                // The user token cookie match with the token in DB.
                if ($activeSession->getToken() === $tokenFromCookie) {
                    if ($activeSession->isBlocked()) {
                        $this->blockAccess($event, $activeSession);
                    }
                    // Authorized to access...
                    return;
                }
            }
        }

        // We didn't found an active session for the user in the database, create a new one
        $newActiveSession = $this->activeSessionManager->new($user);

        // Save the token for the cookie
        $this->tokenForTheCookie = $newActiveSession->getToken();

        // Set the cookie to the current user to continue the process as normal
        $request->cookies->set($this->cookieName, $newActiveSession->getToken());

        // Checking if the new active session is in the authorized limit
        $activeSessions[] = $newActiveSession;

        if ($this->sessionLimiter->isLimitReached($activeSessions)) {
            $this->activeSessionManager->block($newActiveSession);
            $this->eventDispatcher->dispatch(
                new ActiveSessionHasReachedLimitEvent($newActiveSession), ActiveSessionHasReachedLimitEvent::NAME
            );
            if ($newActiveSession->isBlocked()) {
                $this->blockAccess($event, $newActiveSession);
            }
        }

        // Authorized to access...
    }

    public function setCookie($event): void
    {
        if (! \is_string($this->tokenForTheCookie)) {
            return;
        }

        $cookie = new Cookie($this->cookieName, $this->tokenForTheCookie, 2147483647);
        $event->getResponse()->headers->setCookie($cookie);
    }

    /**
     * Checking if the current route is excluded
     */
    private function routeOrControllerIsExcluded(Request $request): bool
    {
        // Check for controllers
        foreach ($this->excluded as $controllerClass) {
            if (false !== strpos($request->get('_controller'), $controllerClass)) {
                return true;
            }
        }

        // Check for routes
        return \in_array($request->get('_route'), $this->excluded);
    }

    /**
     * Block access with a 403 error
     */
    private function blockAccess($event, ActiveSessionInterface $activeSession): void
    {
        $context = [];
        if ($event->getRequest()->isXmlHttpRequest()) {
            $context['fromXmlHttpRequest'] = true;
        }

        $response = new Response($this->twig->render('@IpnozActiveSession/limit-reached.html.twig', $context), 403);

        $event->setResponse($response);
        $event->stopPropagation();

        $this->eventDispatcher->dispatch(new AccessBlockedEvent($activeSession), AccessBlockedEvent::class);
    }
}
