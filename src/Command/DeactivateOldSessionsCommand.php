<?php

/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\ActiveSessionBundle\Command;

use Ipnoz\ActiveSessionBundle\DependencyInjection\Configuration;
use Ipnoz\ActiveSessionBundle\Entity\ActiveSessionManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class DeactivateOldSessionsCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        parent::configure();
        $this->setName('ipnoz:active-session:deactivate-olds');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $days = $this->getContainer()->getParameter(Configuration::ROOT_NAME.'.command.deactivate_olds_in_days');

        /** @var ActiveSessionManager $manager */
        $manager = $this->getContainer()->get(ActiveSessionManager::class);
        $manager->deactivateOlds('-'.$days.' days');
    }
}
