<?php

/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\ActiveSessionBundle\Entity;

use Doctrine\ORM\EntityManagerInterface;
use Ipnoz\ActiveSessionBundle\Event\ActiveSessionCreatedEvent;
use Ipnoz\ActiveSessionBundle\Model\ActiveSessionInterface;
use Ipnoz\ActiveSessionBundle\Repository\ActiveSessionRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ActiveSessionManager
{
    private $em;
    private $requestStack;
    private $eventDispatcher;

    private $userClass;
    private $activeSessionClass;
    private $cookieName;
    private $blockDelay;

    /** @var ActiveSessionRepository */
    private $repository;

    public function __construct(
        EntityManagerInterface $em,
        RequestStack $requestStack,
        EventDispatcherInterface $eventDispatcher,
        string $userClass,
        string $activeSessionClass,
        string $cookieName,
        int $blockDelay
    ) {
        $this->em = $em;
        $this->requestStack = $requestStack;
        $this->eventDispatcher = $eventDispatcher;
        $this->userClass = $userClass;
        $this->activeSessionClass = $activeSessionClass;
        $this->cookieName = $cookieName;
        $this->blockDelay = $blockDelay;

        $this->repository = $em->getRepository($activeSessionClass);
    }

    /**
     * Create and persist a new ActiveSession
     */
    public function new($user): ActiveSessionInterface
    {
        if (\get_class($user) !== $this->userClass) {
            throw new \InvalidArgumentException(
                \sprintf('Invalid user class. Expected "%s", but got "%s".', $this->userClass, \get_class($user))
            );
        }

        $request = $this->requestStack->getCurrentRequest();

        if (! $request instanceof Request) {
            throw new \RuntimeException('You can execute this command only with a HTTP request');
        }

        /** @var ActiveSessionInterface $activeSession */
        $activeSession = new $this->activeSessionClass();
        $activeSession->setUser($user);
        $activeSession->setCreatedAt(new \DateTime());
        $activeSession->setIp($request->server->get('REMOTE_ADDR'));
        $activeSession->setUserAgent($request->server->get('HTTP_USER_AGENT'));

        $this->em->persist($activeSession);
        $this->em->flush();

        $this->eventDispatcher->dispatch(
            new ActiveSessionCreatedEvent($activeSession), ActiveSessionCreatedEvent::NAME
        );

        return $activeSession;
    }

    public function block(ActiveSessionInterface $activeSession): void
    {
        if ($this->blockDelay > 0) {
            $activeSession->setBlockedAt(new \DateTime('now +'.$this->blockDelay.' days'));
        } else {
            $activeSession->block();
        }

        $this->em->flush();
    }

    public function deactivateOlds(string $datetime)
    {
        $this->repository->deactivateOlds($datetime);
    }

    /**
     * Find the ActiveSession from the cookie of the request
     */
    public function findFromCookie(): ?ActiveSessionInterface
    {
        $request = $this->requestStack->getCurrentRequest();

        if (! $request instanceof Request) {
            return null;
        }

        if (! $request->cookies->has($this->cookieName)) {
            return null;
        }

        $token = $request->cookies->get($this->cookieName);

        return $this->repository->findOneBy(['token' => $token]);
    }
}
