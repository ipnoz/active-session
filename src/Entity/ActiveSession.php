<?php

/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\ActiveSessionBundle\Entity;

use Ipnoz\ActiveSessionBundle\Model\ActiveSessionInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ActiveSession implements ActiveSessionInterface
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @var string
     */
    protected $token;

    /**
     * @var string
     */
    protected $ip;

    /**
     * @var string
     */
    protected $userAgent;

    /**
     * @var \DateTime
     */
    protected $blockedAt;

    /**
     * @var \DateTime
     */
    protected $deactivatedAt;

    /**
     * @var \DateTime
     */
    protected $createdAt;


    public function __construct()
    {
        $this->createToken();
    }

    /**
     * @inheritDoc
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @inheritDoc
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @inheritDoc
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @inheritDoc
     */
    public function getIp(): ?string
    {
        return $this->ip;
    }

    /**
     * @inheritDoc
     */
    public function setIp($ip): void
    {
        $this->ip = $ip;
    }

    /**
     * @inheritDoc
     */
    public function getUserAgent(): ?string
    {
        return $this->userAgent;
    }

    /**
     * @inheritDoc
     */
    public function setUserAgent($userAgent): void
    {
        $this->userAgent = $userAgent;
    }

    /**
     * @inheritDoc
     */
    public function getBlockedAt(): ?\DateTime
    {
        return $this->blockedAt;
    }

    /**
     * @inheritDoc
     */
    public function setBlockedAt(?\DateTime $blockedAt): void
    {
        $this->blockedAt = $blockedAt;
    }

    /**
     * @inheritDoc
     */
    public function isBlocked(): bool
    {
        return ($this->blockedAt instanceof \DateTime && $this->blockedAt->getTimestamp() < \time());
    }

    /**
     * @inheritDoc
     */
    public function block(): void
    {
        $this->blockedAt = new \DateTime('now -2 seconds');
    }

    /**
     * @inheritDoc
     */
    public function unblock(): void
    {
        $this->blockedAt = null;
    }


    /**
     * @inheritDoc
     */
    public function getDeactivatedAt(): ?\DateTime
    {
        return $this->deactivatedAt;
    }

    /**
     * @inheritDoc
     */
    public function setDeactivatedAt(?\DateTime $deactivatedAt): void
    {
        $this->deactivatedAt = $deactivatedAt;
    }

    /**
     * @inheritDoc
     */
    public function isActive(): bool
    {
        return (! $this->deactivatedAt instanceof \DateTime || $this->deactivatedAt->getTimestamp() > \time());
    }

    /**
     * @inheritDoc
     */
    public function isDeactivated(): bool
    {
        return ($this->deactivatedAt instanceof \DateTime && $this->deactivatedAt->getTimestamp() < \time());
    }

    /**
     * @inheritDoc
     */
    public function deactivate(): void
    {
        $this->deactivatedAt = new \DateTime('now -2 seconds');
    }

    /**
     * @inheritDoc
     */
    public function activate(): void
    {
        $this->deactivatedAt = null;
    }

    /**
     * @inheritDoc
     */
    public function willBeBlocked(): bool
    {
        return (! \is_null($this->getBlockedAt()) && ! $this->isBlocked());
    }

    /**
     * @inheritDoc
     */
    public function isAuthorizedSession(): bool
    {
        return ($this->isActive() && \is_null($this->getBlockedAt()));
    }

    /**
     * @inheritDoc
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * @inheritDoc
     */
    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @inheritDoc
     */
    protected function createToken()
    {
        $this->token = \base64_encode(\random_bytes(64));
    }
}
