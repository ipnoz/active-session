<?php

/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\ActiveSessionBundle\Entity;

use Ipnoz\ActiveSessionBundle\Model\UserInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
abstract class User implements UserInterface
{
}
