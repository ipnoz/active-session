<?php

/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\ActiveSessionBundle\SessionLimiter;

use Ipnoz\ActiveSessionBundle\Model\ActiveSessionInterface;

/**
 * Handle the session limiter
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class SessionLimiterHandler
{
    /** @var SessionLimiterInterface $limiter */
    private $limiter;

    /** @var int $limit */
    private $limit;

    public function setLimiter(SessionLimiterInterface $limiter)
    {
        $this->limiter = $limiter;
    }

    public function getLimit(): int
    {
        if (\is_null($this->limit)) {
            $this->limit = $this->limiter->getLimit();
        }
        return $this->limit;
    }

    public function isIllimited(): bool
    {
        return (0 === $this->getLimit());
    }

    /**
     * @param ActiveSessionInterface[] $activeSessions
     */
    public function isLimitReached(array $activeSessions): bool
    {
        if ($this->isIllimited()) {
            return false;
        }

        $authorizedSessions = [];
        foreach ($activeSessions as $activeSession) {
            if ($activeSession->isAuthorizedSession()) {
                $authorizedSessions[] = $activeSession;
            }
        }

        return (\count($authorizedSessions) > $this->getLimit());
    }

    public function regularization(array $activeSessions): bool
    {
        if ($this->isLimitReached($activeSessions)) {
            return false;
        }

        $totalToRegularize = $this->getLimit() - $this->countAuthorizedSessions($activeSessions);

        return $this->regularize($activeSessions, $totalToRegularize);
    }

    private function regularize(array $activeSessions, int $limit = null): bool
    {
        $requireFlush = false;
        $i = 0;

        foreach ($activeSessions as $activeSession) {
            if ($activeSession->willBeBlocked()) {
                $activeSession->unblock();
                $requireFlush = true;
                ++$i;
            }
            if ($limit === $i) {
                break;
            }
        }

        return $requireFlush;
    }

    private function countAuthorizedSessions(array $activeSessions): int
    {
        $count = 0;

        foreach ($activeSessions as $activeSession) {
            if ($activeSession->isAuthorizedSession()) {
                ++$count;
            }
        }

        return $count;
    }
}
