<?php

/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\ActiveSessionBundle\SessionLimiter;

/**
 * Return the limit set with the constructor
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class SimpleSessionLimiter implements SessionLimiterInterface
{
    private $maxSession;

    public function __construct(int $maxSession)
    {
        $this->maxSession = $maxSession;
    }

    public function getLimit(): int
    {
        return $this->maxSession;
    }
}
