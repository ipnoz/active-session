<?php

/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\ActiveSessionBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use Ipnoz\ActiveSessionBundle\Service\SessionAuthorizeService;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class SessionAuthorizeListener
{
    private $service;
    private $entityManager;

    public function __construct(
        SessionAuthorizeService $service,
        EntityManagerInterface $entityManager
    ) {
        $this->service = $service;
        $this->entityManager = $entityManager;
    }

    public function onKernelRequest($event): void
    {
        $this->service->checkRequest($event);
    }

    public function onKernelResponse($event): void
    {
        $this->service->setCookie($event);
    }
}
