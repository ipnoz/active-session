<?php

/*
 * This file is part of the IpnozActiveSessionbundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\ActiveSessionBundle\Model;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
interface ActiveSessionInterface
{
    public function getToken(): string;

    public function getUser();
    public function setUser($user): void;

    public function getIp(): ?string;
    public function setIp($ip): void;

    public function getUserAgent(): ?string;
    public function setUserAgent($userAgent): void;

    public function getBlockedAt(): ?\DateTime;
    public function setBlockedAt(?\DateTime $limitReachedAt): void;
    public function isBlocked(): bool;
    public function block(): void;
    public function unblock(): void;

    public function getDeactivatedAt(): ?\DateTime;
    public function setDeactivatedAt(?\DateTime $deactivatedAt): void;
    public function isActive(): bool;
    public function isDeactivated(): bool;
    public function activate(): void;
    public function deactivate(): void;
    /**
     * Checking if the session will be blocked in the futur
     */
    public function willBeBlocked(): bool;

    /**
     * Checking if the session is authorized for the session limiter
     */
    public function isAuthorizedSession(): bool;

    public function getCreatedAt(): ?\DateTime;
    public function setCreatedAt($createdAt): void;
}
